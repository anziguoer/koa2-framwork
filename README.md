# koa2-framwork

#### 项目介绍
基于 koa2 实现的 mvc 框架， 项目使用 typescript 开发。

#### 软件架构
> koa2 + mongodb + nginx + linux

#### 安装教程

1. `git clone https://gitee.com/anziguoer/koa2-framwork.git `
2. 安装模块: `npm install `
3. 安装typescript: `npm install -g typescript `

#### 使用说明

1. 编译文件 `tsc -w`
2. 编译并启动 `npm run build`  开发环境使用.
3. 启动项目 `npm start ` or `NODE_ENV=production pm2 start app/index.js --name=name`

#### 目录说明
```
├── app                     // 编译后项目文件目录
├── public                  // 前端静态文件
│   ├── css
│   ├── img
│   └── js
├── src                     // 项目 ts 源码目录
│   ├── admin               // 管理端模块
│   │   ├── controller      // 后台控制器目录
│   │   ├── module          // 后台 module目录
│   │   ├── schema          // 数据库 schema目录
│   │   └── service         // 后台服务文件目录
│   ├── config              // 项目配置目录
│   ├── core                // 核心文件夹
│   │   └── db              // 数据库启动
│   ├── home                // 用户端模块
│   │   ├── controller      // 用户端控制器目录
│   │   ├── module
│   │   ├── schema
│   │   └── service
│   ├── logs                // 日志文件目录
│   └── route               // 路由文件目录
|   |__ util                // 工具目录
└── views                   // 视图目录
    ├── admin               // 管理端视图目录
    └── home                // 用户端视图目录
```

#### 前端人员请求接口说明:
所有的 `post`, `delete`, `put` 请求， 需要携带`_csrf`参数, 如果没有此参数， 以上请求不予通过
> 用户端的csrf 参数设置: 添加`<meta name="csrf-token" content="{{ _csrf }}">`到页面的header中。`_csrf`参数发送形式有如下几种方式：

1. body 中添加
```
{
    "_csrf" : "",
    "name" : "张三",
    
    '''
}
```
2. 在http请求的header中发送：`csrf-token` 或者 `xsrf-token ` 或者 `x-csrf-token` 或者 `x-xsrf-token`

> 管理端csrf参数：请求接口： `get` 请求 `/api/scrf` 获取

#### 项目规范

[阅读项目规范](./readme/项目规范.md)

#### 参与贡献

1. Fork 本项目
2. 新建 Feat_xxx 分支
3. 提交代码
4. 新建 Pull Request
