## model文件须知

1. model类文件继承基类 BaseModel， 才可以使用 mongoose 中的方法。
2. 在 `constructor() {
        super('index');
    }`中传递对应的数据表名称, 
3. 在 schema 文件夹中必须要对应的 同名 schema 文件， 比如: `schema/index.ts`
4. 在使用 model 类的时候， 会自动在 schema 文件夹中找对应的 `index.ts` 文件。