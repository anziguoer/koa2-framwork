/**
 * 首页控制器
 */
import BaseController from "../../core/BaseController";
class Index extends BaseController {
    async index(){
        await this.render('home/index');
    }
}

module.exports = Index;