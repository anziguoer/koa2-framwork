/**
 * 项目入口文件
 */
// 项目根目录
const BASE_PATH = __dirname;
global['BASE_PATH'] = BASE_PATH;
// 项目配置文件目录
global['CONFIG_PATH'] = BASE_PATH + '/config/';

// 项目路由目录
global['ROUTE_PATH'] = BASE_PATH + '/route/';

// 数据库 schema 文件
global['SCHEMA_PATH'] = BASE_PATH + '/schema/';

import config from './config/app';
// 导入项目的启动文件
import start  from './core/app';

// 项目启动
start(config);