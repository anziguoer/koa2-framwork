import BaseController from "../../core/BaseController";
class Index extends BaseController {
    async index(){
        this.render("admin/index");
    }
}

module.extends = Index;