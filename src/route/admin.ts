/**
 * 后台路由配置文件
 */

import { interfaceRoute, method } from "../interface/routeInterface";

// 定义路由
let adminRoute: interfaceRoute = [
    ["/", "index@index", method["get"]],
];



export default adminRoute;