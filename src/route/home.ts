/**
 * 前台路由配置文件
 */
import { method, interfaceRoute } from "../interface/routeInterface";
// 定义路由
let homeRoute: interfaceRoute = [
    ["/", "index@index", method["get"]],
];

export default homeRoute;