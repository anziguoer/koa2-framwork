## 路由目录

### 路由定义

格式：

["/", "index@index", method["get"]]

['路由'， '控制器@方法', 'get|post|delete|put']

### 路由映射

系统会自动映射对应的控制器目录

比如： home.ts 路由会自动在 `home/controller` 的目录中查找控制器文件， 并自动调用 action, 明白原理用户可以自行修改。