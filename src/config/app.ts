/*
 * @Author: 杨玉龙 
 * @Email undefined 
 * @Date: 2018-10-11 15:24:15 
 * @Last Modified by:   杨玉龙 
 * @Last Modified time: 2018-10-11 15:24:15 
 * @Document 项目配置文件
 */

import configInterface from '../interface/configInterface';
import mongodbConfig from "./mongodb";
import redisConfig from "./redis";
import ossConfig from "./oss";
// 定义服务启动的端口
const PORT = process.env.PORT || 3000;
// 定义环境
const ENVIROUMENT = process.env.NODE_ENV || 'development';

/**
 * 合并配置
 */
let config:configInterface;
config = {
    env : ENVIROUMENT,
    port : PORT,
    mongodb : mongodbConfig[ENVIROUMENT],
    oss : ossConfig[ENVIROUMENT],
    redis : redisConfig[ENVIROUMENT],
};

export default config;