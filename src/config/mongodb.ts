/*
 * @Author: 杨玉龙 
 * @Email anziguoer@163.com 
 * @Date: 2018-10-11 15:10:55 
 * @Last Modified by: 杨玉龙
 * @Last Modified time: 2018-10-11 15:11:53
 */

export default {
    'development' : {
        host : 'localhost',
        port : '27017',
        database : '',
        user : '',
        pass : '',
        autoIndex : true
    },
    'production' : {
        host : '127.0.0.1',
        port : '27017',
        database : '',
        user : '',
        pass : '',
        autoIndex : false
    },
}