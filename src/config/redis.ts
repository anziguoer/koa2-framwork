/*
 * @Author: 杨玉龙 
 * @Email undefined 
 * @Date: 2018-10-11 15:15:12 
 * @Last Modified by: 杨玉龙
 * @Last Modified time: 2018-10-11 15:21:30
 * @Document redis 配置
 */

export default {
    "development" : {
        host : "",
        port : 6379,
        user:'',
        pwd : '',
    },
    "production" : {
        host : "",
        port : 6379,
        user:'',
        pwd : '',
    }
}
