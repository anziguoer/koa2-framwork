/**
 * mongoose schema
 */
const aboutSchema = {
    title : {type : String, required:true, trim:true},
    value : {type : String, required:true, default:'', trim: true},
    is_deleted : {type: Boolean, default : false},
};

module.exports = aboutSchema;