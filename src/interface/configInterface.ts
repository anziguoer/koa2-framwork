
// mongodb 配置
interface mongodbConfig {
    uri ?:string,
    host? :string,
    port? : string,
    database? : string,
    user? : string,
    pass? : string,
    autoIndex : true|false,
};

/**
 * oss 配置
 */
interface ossConfig {
    accessId : string,
    accessKey : string,
    [propName:string]:any,
}
/**
 * reids 配置
 */
interface redisConfig {
    "ip": string,
    "port": number,
    "option" ?: object,
}

/**
 * 定义系统配置信息的接口
 */
interface configInterface {
    env : string;
    port : string|number;
    mongodb : {
        'development' : mongodbConfig,
        'production' : mongodbConfig,
    },
    oss? : ossConfig;
    redis? : redisConfig,
}


export default configInterface;
