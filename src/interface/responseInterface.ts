/**
 * 定义相应的接口
 */
interface ResponseInterface {
    data?: Object | Array<String>,
    msg?: String,
    code: Number,
}

/**
 * 成功的响应
 */
interface SuccessResponse extends ResponseInterface {
    data : Object | Array<string|number>,
    msg : 'success',
    code : 0
}

/**
 * 失败的响应
 */
interface ErrorResponse extends ResponseInterface {
    msg: String,
    code: Number,
}
/**
 * 用户未登录响应
 */
interface UnauthoziedResponse extends ResponseInterface {
    readonly code : 401,
    msg : "用户未登录",
}

/**
 * 无权限的响应
 */
interface ForbiddenResponse extends ResponseInterface {
    readonly code : 403,
    msg : "无权限"
}

export { ResponseInterface, SuccessResponse, ErrorResponse, UnauthoziedResponse, ForbiddenResponse};