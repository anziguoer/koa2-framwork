/**
 * 分页器接口
 */

export default interface paginateInterface {
    page : number,
    page_size : number,
    total? : number,
 }