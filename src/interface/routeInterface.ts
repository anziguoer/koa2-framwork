
export enum method {
    get = 'get',
    post = 'post',
    put = 'put',
    delete = 'delete',
    options = 'options'
}

export interface interfaceRoute {
    [index:number] : [string, string, method]
}
