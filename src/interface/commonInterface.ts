/**
 * common 文件公共接口
 */

/**
 * oss 签名接口
 */
interface ossSign { 
    expiration: number,
    contentLength: number,
    accessId: string,
    accessKey: string
} 

export { ossSign };