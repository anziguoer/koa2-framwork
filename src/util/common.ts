import { ossSign } from "../interface/commonInterface";
import crypt from "crypto";

/**
 * 密码加密函数
 * @param data
 * @param key
 */
function makePass(data:string, key:string){
    let hmac = crypt.createHmac('md5',key);
    return hmac.update(data).digest('base64');
}
/**
 * 首字母大写
 * @param str 
 */
function ucfirst(str) {
    if (!str) {
        return '';
    }
    return (str.substr(0, 1)).toUpperCase() + str.substr(1);
}

/**
 * oss 签名函数
 * @param params 
 */
function ossSign(params: ossSign) {
    var expiration = new Date();
    expiration.setMilliseconds(expiration.getMilliseconds() + params.expiration);
    var policyBase64 = new Buffer(JSON.stringify({
        expiration: expiration,
        conditions: [['content-length-range', 0, params.contentLength]]
    })).toString('base64');
    
    return {
        policy: policyBase64,
        accessId: params.accessId,
        signature: crypt.createHmac('sha1', params.accessKey).update(policyBase64).digest().toString('base64')
    };
}

// 导出所有的函数
export { makePass, ucfirst, ossSign};