import validators from "./validators/validators";

/**
 * 验证属性
 */
interface ValidatorOptions {
    continue: boolean,
}
/**
 * 验证器
 */
class Validator {
    /**
     * 数据验证的规则
     */
    rules: object = {};

    /**
     * 记录错误信息
     */
    private messagesArray: Array<string> = [];

    /**
     * 验证器配置项
     */
    options: ValidatorOptions = { continue: false }; //初始化值
    constructor(options?: ValidatorOptions) {
        Object.assign(this.options, options);
    }

    /**
     * 验证规则
     */
    rule(rule: object) {
        this.rules = rule;
        return this;
    }

    /**
     * 具体的验证逻辑
     * @param params
     * @returns boolean
     */
    validate(params: object): Boolean {
        let is_boolean = true;
        try {
            // 1 遍历 rules
            for (const column in this.rules) {
                let rule = this.rules[column];
                // 如果是数组， 则按规则是使用了自定义函数验证
                if (Array.isArray(rule)) {
                    let [callback, msg = `${column}验证失败`] = rule;
                    if (typeof callback !== "function") {
                        // 如果第一个参数不是一个fun， 则不处理验证
                        continue;
                    }
                    if (!this.doCustomValidate(callback, params[column])) {
                        is_boolean = false;
                        if (!this.options.continue) {
                            throw new Error(msg);
                        }
                        this.messagesArray.push(msg);
                    }
                } else {
                    for (const validator in this.rules[column]) {
                        let validatorArray: Array<String> = validator.split("|");
                        let msg = rule[validator] || `${column}验证失败`;
                        // 多条件验证，有一个通过则通过
                        if (validatorArray.length > 1) {
                            let tmp_is_boollean = false;
                            for (const index in validatorArray) {
                                if (!validators.get(validatorArray[index])) {
                                    continue; // 如果没有规则， 则不验证
                                }
                                tmp_is_boollean = validators.get(validatorArray[index])(params[column]) || tmp_is_boollean;
                                is_boolean = tmp_is_boollean;
                            }
                            if (!is_boolean) {
                                if (!this.options.continue) {
                                    throw new Error(msg);
                                }
                                this.messagesArray.push(msg);
                            }
                        } else {
                            if (!validators.get(validatorArray[0])) {
                                continue; // 如果没有规则， 则不验证
                            }
                            // 执行预定义规则的验证
                            if (!validators.get(validatorArray[0])(params[column])) {
                                is_boolean = false;
                                if (!this.options.continue) {
                                    throw new Error(msg);
                                }
                                this.messagesArray.push(msg);
                            }
                        }
                    }
                }
            }
        } catch (error) {
            this.messagesArray.push(error.message);
        }
        return is_boolean;
    }

    /**
     * 使用自定义验证
     */
    private doCustomValidate(callback: Function, value: any): Boolean {
        return callback.call(this, value);
    }

    /**
     *
     * 记录错误信息
     */
    get messages(): Array<string> {
        console.log(this.messagesArray);
        return this.messagesArray;
    }
}

export default Validator;

/**
 * 使用说明
 */
// let validator = new Validator({ continue: false }); //初始化
// // 验证的数据
// let body =
// {
//     mobile: "18611325483",
//     name: 'asdfasdf',
//     age: 19,
//     telphone : '010-1122303'
// }
// // =============== 设置验证规则 =================
// let test = {
//     mobile: { // 字段
//         'mobile|fixed_telephone': "不合法的手机号码", //用mobile或者fixed_telephone的规则去验证
//         'required': "手机号码不能为空", // 不能为空的验证
//     },
//     // name: {
//     //     'required': '用户名不能为空',
//     // },
//     age: [function (val) { return parseInt(val) > 20; }, "heheh"], //自定义验证[function:boolean, msg:错误信息
// };
// validator.rule(test).validate(body); //验证调用, 返回 true 或者false

// console.log(validator.messages) // 获取错误信息

