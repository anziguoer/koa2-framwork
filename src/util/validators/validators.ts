/**
 * 验证接口
 */
interface ValidatorInterface {
    (value: any): boolean;
}
let validators: Map<String, ValidatorInterface> = new Map();

/**
 * 手机号码验证规则
 * @returns boolean
 */
validators.set('mobile', (value: any): boolean => {
    let reg = /^((1[3,8][0-9])|(14[5,7])|(15[0-3,5-9])|(17[0,3,5-8])|166|(19[8,9]))\d{8}$/;
    return reg.test(value);
});

/**
 * 固定电话验证
 */
validators.set('fixed_telephone', (value: any) => {
    var reg = new RegExp(/^\d{3}-\d{7,8}|\d{4}-\d{7,8}$/);
    return reg.test(value);
});

/**
 * 必填验证
 * @returns boolean 
 */
validators.set('required', (value: any = null): boolean => {
    return Boolean(value);
});

/**
 * 邮箱验证
 */
validators.set('email', (value: any): boolean => {
    let reg = new RegExp(/^[a-zA-Z0-9_.-]+@[a-zA-Z0-9-]+(\.[a-zA-Z0-9-]+)*\.[a-zA-Z0-9]{2,6}$/);
    return reg.test(value);
});

/**
 * 邮编验证 6位数字
 */
validators.set('postcode', (value: any): boolean => {
    let reg = new RegExp(/\d{6}/);

    return reg.test(value);
 })
export default validators;