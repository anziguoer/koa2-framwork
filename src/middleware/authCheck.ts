/**
 * 用户权限判断中间件
 * @param ctx
 * @param next
 */
import * as response from "../interface/responseInterface";

/**
 * 定义路由白名单
 */
let routerWhiteList:Array<string>;
routerWhiteList = []; // 路由白名单
export default async (ctx, next) => {
    let url = ctx.req.url;
    // 如果请求的路由中没有api的则全部放行
    if (url.indexOf('api') < 0) {
        await next();
    } else if (routerWhiteList.includes(url)) {
        // 白名单中的路由放行
        await next();
    } else if (ctx.session && ctx.session.user) {
        // 用户登录了， 则放行
        await next();
    } else {
        let responseData: response.UnauthoziedResponse;
        responseData = {
            code : 401, //未授权, 其他情况请勿暂用此状态码
            msg : "用户未登录",
        };
        ctx.body = responseData;
    }
}