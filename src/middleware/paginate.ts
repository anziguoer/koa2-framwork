/**
 * 分页器
 * @param ctx 
 * @param next 
 */
import paginateInterface from "../interface/paginateInterface";
export default async function(ctx, next){
    let paginate : paginateInterface = {
        page: parseInt(ctx.query.page) || 1,
        page_size: parseInt(ctx.query.page_size) || 10,
    };
    ctx.paginate = paginate;
    await next();
} 